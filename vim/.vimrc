syntax on

set number
set relativenumber
set noexpandtab
set autoindent
set hlsearch
set showmatch

colorscheme default

autocmd FileType c inoremap <buffer> ( ()<Left>
autocmd FileType c inoremap <buffer> <expr> ) getline('.')[col('.') - 1] == ')' ? '<Right>' : ')'
autocmd FileType c inoremap <buffer> [ []<Left>
autocmd FileType c inoremap <buffer> <expr> ] getline('.')[col('.') - 1] == ']' ? '<Right>' : ']'
autocmd FileType c inoremap <buffer> { {}<Left>
autocmd FileType c inoremap <buffer> <expr> } getline('.')[col('.') - 1] == '}' ? '<Right>' : '}'
autocmd FileType c inoremap <buffer> {<CR> {}<Left><CR><CR><Up><Tab>
autocmd FileType c inoremap <buffer> " ""<Left>
