#!/bin/bash

xrandr --output HDMI-1 \
        --mode 1920x1080 \
        --scale-from 2560x1440 \
        --pos 0x0

xrandr --output eDP-1 \
        --mode 2560x1440 \
        --pos 0x1440
