#!/bin/bash

xrandr --output HDMI-1 \
        --off

xrandr --output eDP-1 \
        --mode 2560x1440 \
        --pos 0x0
