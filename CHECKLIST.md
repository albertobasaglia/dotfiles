# Checklist

- Enable paccache.timer to clear the package cache weekly
        - Provided by: pacman-contrib
- If using an SSD, enable fstrim.timer to discard unused blocks periodically
- Setup a firewall such as ufw or firewalld
- Install and configure reflector to frequently update the mirrorlist automatically
- Enable in /etc/pacman.conf
        - Color
        - ILoveCandy
        - ParallelDownloads = 5
- Install intel-ucode or amd-ucode microcode depending on your CPU
- For laptops, setup CPU frequency scaling and optimise battery life with:
        - tlp
        - autocpu-freq
        - powertop
        - power-profiles-daemon
- Install a backup kernel like LTS or Zen kernel
- For NVIDIA users, create a pacman hook to ensure initramfs gets updated
- Install noto-fonts for basic font coverage
