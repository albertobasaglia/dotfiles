let mapleader = " "
call plug#begin()
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Plug 'itchyny/lightline.vim'
" Plug 'scrooloose/syntastic'
" Plug 'townk/vim-autoclose'
Plug 'jiangmiao/auto-pairs'
Plug 'liuchengxu/vim-which-key'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'morhetz/gruvbox'
" Plug 'EdenEast/nightfox.nvim'
" Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
Plug 'navarasu/onedark.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'majutsushi/tagbar'
Plug 'lervag/vimtex'
Plug 'sirver/ultisnips'
" Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree'
Plug 'tikhomirov/vim-glsl'
call plug#end()
colorscheme gruvbox
let g:airline_powerline_fonts = 1
let g:coc_global_extensions = ['coc-clangd', 'coc-vimtex']
set relativenumber number
set noerrorbells
set expandtab
set shiftwidth=8
set tabstop=8
set softtabstop=8
set scrolloff=8
set signcolumn=yes
set completeopt-=preview
set incsearch
set colorcolumn=80
set updatetime=300

" snippets stuff
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my_snippets"]

" which-key stuff
let g:which_key_fallback_to_native_key=1
set timeoutlen=500
nnoremap <silent> <leader> :WhichKey ','<CR>
nnoremap <silent> g :WhichKey 'g'<CR>
" nnoremap <silent> t :WhichKey 't'<CR>
" nnoremap <silent> d :WhichKey 'd'<CR>
" nnoremap <silent> c :WhichKey 'c'<CR>
nnoremap <silent> \ :WhichKey '\'<CR>
nnoremap <silent> ] :WhichKey ']'<CR>
nnoremap <silent> [ :WhichKey '['<CR>

nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> gh :CocDiagnostics<CR>
nmap <silent> gs :CocCommand clangd.switchSourceHeader<CR>
nmap <silent> gf <Plug>(coc-fix-current)
nmap <silent> gr <Plug>(coc-rename)
nmap <silent> gb <Plug>(coc-format)

inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

nnoremap <silent> K :call ShowDocumentation()<CR>
function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

nnoremap <leader>tt <cmd>TagbarToggle<CR>
nnoremap <leader>tf <cmd>TagbarOpen j<CR>
nnoremap <leader>nt :NERDTreeToggle<CR>
nnoremap <leader>nf :NERDTreeFocus<CR>

let g:airline#extensions#whitespace#mixed_indent_algo = 2
let g:vimtex_view_general_viewer = 'zathura'
